/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (c) 2013, Francisco Suárez
 * All rights reserved.
 */

#include "Bola.h"

Bola::Bola(Point2f posicion, Point2f velocidad, float radio)
: Figura(posicion, velocidad) {
	m_radio = radio;
	m_color = AZUL;
}

Bola::~Bola() {
	// [Ball] Something to destroy?
}

float Bola::getRadio() {
	return m_radio;
}

void Bola::setRadio(float radio) {
	m_radio = radio;
}

void Bola::update(Rectf &clipArea) {
	m_posicion += m_velocidad;
	// Check if the ball exceeds the edges
	float xMin = clipArea.x + m_radio;
	float xMax = clipArea.br().x - m_radio;
	float yMin = clipArea.y + m_radio;
	float yMax = clipArea.br().y - m_radio;
	if (m_posicion.x > xMax) {
		m_posicion.x = xMax;
		m_velocidad.x = -m_velocidad.x;
	} else if (m_posicion.x < xMin) {
		m_posicion.x = xMin;
		m_velocidad.x = -m_velocidad.x;
	}
	if (m_posicion.y > yMax) {
		m_posicion.y = yMax;
		m_velocidad.y = -m_velocidad.y;
	} else if (m_posicion.y < yMin) {
		m_posicion.y = yMin;
		m_velocidad.y = -m_velocidad.y;
	}
}

void Bola::dibujar() {
	glLoadIdentity();              								// Reset model-view matrix
	glTranslatef(0.0f, 0.0f, 0.0f);  							// Translate to (xPos, yPos, zPos)
	// Use triangular segments to form a circle
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0f, 0.0f, 1.0f);  // Blue
	glVertex2f(0.0f, 0.0f);       // Center of circle
	int numSegments = 100;
	GLfloat angle;
	for (int i = 0; i <= numSegments; i++) { 						// Last vertex same as first vertex
		angle = i * 2.0f * M_PI / numSegments;  					// 360 deg dived in all segments
		glVertex2f(cos(angle) * m_radio, sin(angle) * m_radio);
	}
	glEnd();
}

bool Bola::colision(Bloque &figura) {
	// Check for collision with the brick
	Rectf brick_rect = figura.getRectangle();
	Rectf collide_rect = brick_rect + Size2f(2*m_radio, 2*m_radio);
	collide_rect -= Point2f(m_radio, m_radio);
	// If no collision return false
	if (!collide_rect.contains(m_posicion))
		return false;
	float xLeft = abs(m_posicion.x - collide_rect.x);
	float xRigth = abs(m_posicion.x - collide_rect.br().x);
	float yDown = abs(m_posicion.y - collide_rect.y);
	float yTop = abs(m_posicion.y - collide_rect.br().y);
	Vec4f values(xLeft, xRigth, yDown, yTop);
	int minIdx;
	minMaxIdx(values, NULL, NULL, &minIdx, NULL);
	if ((minIdx == 0) || (minIdx == 1))
		m_velocidad.x = -m_velocidad.x;
	if ((minIdx == 2) || (minIdx == 3))
		m_velocidad.y = -m_velocidad.y;
	return true;
}
