/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (c) 2013, Francisco Suárez
 * All rights reserved.
 */

#include "Juego.h"

/** Callbacks for using OpenGL API inside the class (C API) **/
Juego* p_this;
void draw_cb() {
	p_this->dibujaJuego(); }

void update_cb(int value) {
	p_this->mainLoop(); }

void keyPressed_cb(unsigned char key, int x, int y) {
	p_this->movTeclado(key); }

void resize_cb(int w, int h) {
	p_this->dimensiona(w, h); }
/**********************************************************/
/********************* Helper Methods *********************/
string int2str(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}
/**********************************************************/

using namespace cv;

Juego::Juego(int argc, char** argv, int width, int height) {
	// Initialize member attributes
	m_width = width;
	m_height = height;
	m_aspect = m_width / m_height;
	m_cols = m_rows = 15;
	m_clipArea = Rectf(-1.0f, 1.0f, 2.0f, 2.0f);
	m_bloques = new vector<Bloque>;
	// Initialize the ball
	m_bola = new Bola(Point2f(0.0f,-0.8f), Point2f(0.04f, 0.02f), 0.05f);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(m_width, m_height);
	glutCreateWindow("Arkanoid Clone");
	p_this = this;
	glutDisplayFunc(draw_cb);
	glutKeyboardFunc(keyPressed_cb);
	glutReshapeFunc(resize_cb);
	glutTimerFunc(5,update_cb,0);
	glutFullScreen();             // Put into full screen
}

Juego::~Juego() {
}

void Juego::dibujaJuego() {
	glClear(GL_COLOR_BUFFER_BIT);  	// Clear the color buffer
	glMatrixMode(GL_MODELVIEW);    	// To operate on the model-view matrix
	glLoadIdentity();              	// Reset model-view matrix
	m_bola->dibujar();
	for (uint i = 0; i < m_bloques->size(); i++) {
		m_bloques->at(i).dibujar();
	}
	glutSwapBuffers();  			// Swap front and back buffers (of double buffered mode)
}

void Juego::mainLoop() {
	m_bola->update(m_clipArea);
	for (uint i = 0; i < m_bloques->size(); i++) {
		m_bloques->at(i).update();
		if (m_bola->colision(m_bloques->at(i)))
			m_bloques->erase(m_bloques->begin()+i);
	}
	glutPostRedisplay(); 			// Inform GLUT that the display has changed
	p_this = this;
	glutTimerFunc(1,update_cb,0);	//Call update after each 5 millisecond
}

void Juego::movTeclado(unsigned char key) {
	switch (key) {
	case 27: 		// Esc key
		exit(0);
	}
}

void Juego::dimensiona(int width, int height) {
	// Compute aspect ratio of the new window
	if (height == 0) height = 1;                // To prevent divide by 0
	float aspect = (float)width / (float)height;

	// Set the viewport to cover the new window
	glViewport(0, 0, width, height);

	// Set the aspect ratio of the clipping area to match the viewport
	glMatrixMode(GL_PROJECTION);  	// To operate on the Projection matrix
	glLoadIdentity();             	// Reset the projection matrix
	if (width >= height) {			// Width screen
		m_clipArea.x = -1.0f * aspect;
		m_clipArea.width = 2.0f * aspect;
		m_clipArea.y = -1.0f;
		m_clipArea.height = 2.0f;
	} else {
		m_clipArea.x   = -1.0f;
		m_clipArea.width  = 2.0f;
		m_clipArea.y = -1.0f / aspect;
		m_clipArea.height    = 2.0f / aspect;
	}
	gluOrtho2D(m_clipArea.x, m_clipArea.br().x, m_clipArea.y, m_clipArea.br().y);

	m_width = width;
	m_height = height;
	m_aspect = aspect;
	// Builds the 1rst level
	this->buildLevel(1);
}

void Juego::startJuego() {
	glutMainLoop();
}

void Juego::buildLevel(int level) {
	Point2f position;
	Size2f rect_size;
	Rectf brick_rect;
	// Handle the image
	Mat_<int> image;
	String name = "./maps/level_" + int2str(level) + ".bmp";
	image = imread(name, 0);
	if (m_bloques->size() != 0)
			m_bloques->clear();
	// Loops over the map.
	// IMPORTANT: This method is slow but the map is only 15x15 pixels, so It's OK.
	int index = 0;
	for(int row = 0; row < image.rows; ++row) {
		for(int col = 0; col < image.cols; ++col) {
			int pixel = image.at<int>(row,col);
			if (pixel == Bloque::BLANCO)
				continue;
			position = Point2f(m_clipArea.width*col / (float)m_cols, m_clipArea.height*row / (float)m_rows);
			position.x += m_clipArea.x;
			position.y = m_clipArea.br().y - position.y;
			rect_size = Size2f(m_clipArea.width / (float)m_cols, m_clipArea.height / (float)m_rows);
			brick_rect = Rectf(position, rect_size);
			m_bloques->push_back(Bloque(brick_rect, pixel));
			index++;
		}
	}
}
