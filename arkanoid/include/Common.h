#ifndef COMMON_H_
#define COMMON_H_

#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <map>
#include <GL/glut.h>
#include <GL/gl.h>
#include <cv.h>
#include <highgui.h>

const float DEG2RAD = M_PI/180;

using namespace std;
using namespace cv;

typedef Rect_<float> Rectf;

#endif /* COMMON_H_ */
