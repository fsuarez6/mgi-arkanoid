#ifndef FIGURA_H_
#define FIGURA_H_


#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

class Figura
{
public:
	Figura(Vector2f posicion, Vector2f velocidad, Color color);
	virtual ~Figura(void);
	virtual bool update(FloatRect clipArea) = 0;

protected:
	Vector2f  m_posicion;
	Vector2f  m_velocidad;
	Color     m_color;

};

#endif /* FIGURA_H_ */
