#pragma once

#include "Bola.h"
#include "Pala.h"

class Pong
{
public:
	Pong(void);
	virtual ~Pong(void);
	void gameLoop();

private:
	void dibujar();
	void update();
	void eventoTeclado(sf::Event evento);

	RenderWindow *App;
	Bola *m_bola;
	Pala *m_pala;
	FloatRect m_clipArea;
};
