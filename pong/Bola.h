#pragma once
#include "Figura.h"

class Bola : public Figura
{
public:
	Bola(Vector2f posicion, Vector2f velocidad, Color color);
	virtual ~Bola(void);
	virtual bool update(FloatRect clipArea);
	bool colision();
	CircleShape m_circulo;
};
