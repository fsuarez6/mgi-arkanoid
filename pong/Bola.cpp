#include "Bola.h"

Bola::Bola(Vector2f posicion, Vector2f velocidad, Color color) : Figura(posicion, velocidad, color) {
	m_circulo = CircleShape(20.0f);
	m_circulo.setPosition(posicion);
	m_circulo.setFillColor(color);
}

Bola::~Bola(void)
{
}

bool Bola::update(FloatRect clipArea) {
	// Verifica que la bola no se salga de los limites
	if (m_posicion.x > clipArea.width) {
		m_posicion.x = clipArea.width;
		m_velocidad.x = -m_velocidad.x;
	} else if (m_posicion.x < clipArea.left) {
		m_posicion.x = clipArea.left;
		m_velocidad.x = -m_velocidad.x;
	}
	if (m_posicion.y < clipArea.top) {
		m_posicion.y = clipArea.top;
		m_velocidad.y = -m_velocidad.y;
	} else if (m_posicion.y > clipArea.height) {
		m_posicion.y = clipArea.height;
		m_velocidad.y = -m_velocidad.y;
	}
	// Ahora si, actualiza
	m_posicion.x += m_velocidad.x;
	m_posicion.y += m_velocidad.y;
	m_circulo.move(m_velocidad);
	return true;
}

bool Bola::colision() {

}
