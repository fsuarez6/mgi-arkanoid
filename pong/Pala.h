#pragma once
#include "Figura.h"

class Pala : public Figura
{
public:
	Pala(Vector2f posicion, Vector2f dimensiones, Color color);
	virtual ~Pala(void);
	virtual bool update(FloatRect clipArea);
	void setVelocidad(float velY);
	RectangleShape m_rectangulo;

private:
	Vector2f m_dimensiones;
};
