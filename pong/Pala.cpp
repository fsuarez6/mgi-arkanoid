#include "Pala.h"

Pala::Pala(Vector2f posicion, Vector2f dimensiones, Color color) : Figura(posicion, Vector2f(0,0), color) {
	m_rectangulo = RectangleShape(dimensiones);
	m_rectangulo.setFillColor(color);
	m_rectangulo.setPosition(posicion);
	m_dimensiones = dimensiones;
}

Pala::~Pala(void)
{
}

bool Pala::update(FloatRect clipArea) {
	// Verifica que la pala no se salga de los limites
	if (m_posicion.y < clipArea.top) {
		m_posicion.y = clipArea.top;
		m_velocidad.y = 0;
	} else if (m_posicion.y > clipArea.height - m_dimensiones.y) {
		m_posicion.y = clipArea.height - m_dimensiones.y;
		m_velocidad.y = 0;
	}
	// Ahora si, actualiza
	m_posicion.y += m_velocidad.y;
	m_rectangulo.move(m_velocidad);
	return true;
}

void Pala::setVelocidad(float velY) {
	m_velocidad.y = velY;
}
