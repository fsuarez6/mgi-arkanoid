#include "Pong.h"

Pong::Pong(void)
{
	m_clipArea = FloatRect(0, 0, 800, 600);
	// Create the main rendering window
	App = new RenderWindow(VideoMode(800, 600), "Pong");
	Vector2f posicion(m_clipArea.width/2, m_clipArea.height/2);
	Vector2f velocidad(1.0f, 1.2f);
	m_bola = new Bola(posicion, velocidad, Color::Blue);

	Vector2f dimensiones(20, 120);
	posicion.x = m_clipArea.width - dimensiones.x;
	posicion.y = m_clipArea.height/2 - dimensiones.y/2;
	m_pala = new Pala(posicion, dimensiones, Color::White);
}

Pong::~Pong(void)
{
}

void Pong::gameLoop() {
	while (App->isOpen())
    {
        // Procesamos los eventos
        sf::Event evento;
        while (App->pollEvent(evento))
        {
            // Cerrar ventana : exit
            if (evento.type == sf::Event::Closed)
                App->close();
			// Se ha presionado una tecla
			if ((evento.type == sf::Event::KeyPressed) || (evento.type == sf::Event::KeyReleased))
				eventoTeclado(evento);
        }
		// Actualizar todos los elementos del juego
		update();
        // Dibujar los elementos del juego
		App->clear();
        dibujar();
    }
}

void Pong::dibujar() {
	App->draw(m_bola->m_circulo);
	App->draw(m_pala->m_rectangulo);
	App->display();
}

void Pong::update() {
	m_bola->update(m_clipArea);
	m_pala->update(m_clipArea);
}

void Pong::eventoTeclado(Event evento) {
	if (evento.type == sf::Event::KeyPressed)
	{
		if (evento.key.code == sf::Keyboard::Up)
			m_pala->setVelocidad(-0.3f);
		else if (evento.key.code == sf::Keyboard::Down)
			m_pala->setVelocidad(0.3f);
	}
	else
	{
		if ((evento.key.code == sf::Keyboard::Up) || (evento.key.code == sf::Keyboard::Down))
			m_pala->setVelocidad(0.0f);

	}
}
