# Modelado y Gestión de la Información
# Proyecto Final de Curso 2012-2013: Arkanoid Clone

Este repositorio contiene la 'plantilla' que servirá como punto de partida para el desarrollo del proyecto final de curso.

## Que contiene este repositorio

Se entregan dos proyectos:

* `arkanoid` Contiene la implementación de las principales clases así como gran parte de la lógica del programa (colisiones con las paredes y los ladrillos, gestión de los ladrillos y poco más). Dicha lógica es mejorable pero no es el objetivo del proyecto. La parte gráfica esta implementada con **OpenGL**.

* `pong` Contiene una implementación equivalente graficamente a la de `arkanoid` pero utilizando las librerias **SFML 2.0**.

## Clonando el repositorio

La manera más sencilla para obtener el codigo es 'clonar' el repositorio. Para ello basta con ejecutar el siguiente comando:
```
$ git clone https://_fsuarez_@bitbucket.org/_fsuarez_/mgi-arkanoid.git
```
**Windows:** Utilizar un cliente GIT. Ej. [TortoiseGit](http://uncod.in/blog/installing-tortoisegit-on-windows7/)

## Dependencias

Estos dos proyectos han sido compilados en Ubuntu 12.04, 64 bits, utilizando [GCC](http://gcc.gnu.org/) y las siguientes librerias:

`arkanoid`:

* Mesa 9.0 (Equivalente a [OpenGL 3.0](http://www.opengl.org/wiki/Getting_Started))

* [OpenCV 2.4.2](http://opencv.org/downloads.html)

`pong`:

* [SFML 2.0](http://www.sfml-dev.org/download.php)

* [OpenCV 2.4.2](http://opencv.org/downloads.html)


## Sugerencias

* Crear un proyecto desde cero y verificar que se ha configurado correctamente las libreria gráfica (OpenGL o SFML) según corresponda.

* Añadir progresivamente funcionalidades al programa y probarlas una a una. Este enfoque facilita la depuración de errores.

### Roadmap

Un posible roadmap de la implementación de la aplicación puede ser:

1. Crear la clase `Game` y con los métodos necesarios para lanzar la parte gráfica (`draw()`, `keyPressed`, `gameLoop()`)

2. Implementar la clase base de los objetos (Ej. `Figura`)

3. Implementar la clase `Ball` y sus métodos `update()` y `draw()` (El _draw_ inicial debería ser algo sencillo, sin texturas)

4. En este punto se puede integrar la función que lee el mapa y obtiene las posiciones donde se dibujarán los bloques

5. Implementar la clase `Brick` para que se dibujen

6. Implementar la clase `Paddle`

7. En este punto ya deberíamos tener una aplicación funcional con los requisitos mínimos. A partir de aquí todo son _bonus_